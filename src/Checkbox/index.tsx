import React from 'react';
import { TouchableOpacity } from 'react-native';
//import styled from 'styled-components/native';
import Icon from '../Icon'
import V from '../View'
import Txt from '../Text'
import { ThemeProps } from '../Theme';

interface Props {
  label: string;
  listItems?: ListItems[];
  checked?: boolean;
  disabled?: boolean;
  testID?: string;
  headerLabel?: string;
  error?: boolean;
  errorMsg?: string;
  isSelected?: boolean;
  size?: any;
  theme?: ThemeProps;
  onChange?: (item: any) => void;
  onPress: () => any;
}

type ListItems = {
  id: number;
  label: string;
  checked?: boolean;
};

const CheckBoxItem: React.FC<Props> = ({ checked, label, onPress, disabled, error, size, ...props}) => ( 
  <TouchableOpacity style={{flexDirection: 'row', alignItems: 'flex-start'}} onPress={onPress} disabled={disabled} {...props}>     
    <Icon
      size={32}
      color={disabled ? '#999' : error ? '#c20000' : '#363636'}
      name={ checked ? 'checked-square-outline' : 'square-outline'}
      //mr={1}
    />
    <Txt flex={1} mt={'5px'} color={disabled ? 'blacks.2' : error ? 'red' : 'text'}>{label}</Txt>   
  </TouchableOpacity>
)

export default class CheckboxList extends React.Component<Props, {}> {
  state = {
    checked: this.props.isSelected,
    selectAllItems: false,
  };

  selectCurrentItem(id?: number) {
    const { checked } = this.state;
    const { listItems } = this.props;
    if(listItems){
      const index = listItems.findIndex(x => x.id === id);
      listItems[index].checked = !listItems[index].checked;
      this.setState({ listItems });

      if(Object.values(listItems).every(item => item.checked)){
        this.setState({selectAllItems: true});
      } else {
        this.setState({selectAllItems: false});
      }
      this.props.onChange && this.props.onChange(listItems);
    } else {
      this.setState({ checked: !checked });
      this.props.onChange && this.props.onChange(checked)
    }
  }

  selectAllItems = () => {
    const { selectAllItems } = this.state;
    const { listItems } = this.props;
    
    if(listItems){
      listItems.forEach(item => item.checked = !selectAllItems) 
      this.setState({listItems, selectAllItems : !selectAllItems})
      this.props.onChange && this.props.onChange(listItems);
    }
  }

  render() {
    const { checked, selectAllItems } = this.state;
    const { listItems, label, disabled, headerLabel, error, errorMsg, testID, size } = this.props;
    return (
      <V testID={testID}>
        {listItems ? 
        <>
        {headerLabel && 
          <CheckBoxItem 
            label={headerLabel} 
            key={headerLabel} 
            checked={selectAllItems}
            onPress={this.selectAllItems}
            size={size}
            disabled={disabled}
            error={error}/>
        }
        {listItems.map((item: any) => 
          <CheckBoxItem 
            label={item.label} 
            key={item.label} 
            checked={item.checked}
            onPress={() => this.selectCurrentItem(item.id)}
            size={size}
            disabled={disabled}
            error={error}/>
        )}
        </>
        : 
        <CheckBoxItem 
          label={label} 
          key={label}
          checked={checked} 
          onPress={() => this.selectCurrentItem()}
          size={size}
          disabled={disabled}
          error={error}/>
        }
        {errorMsg && <Txt px={'5px'} color="red">{errorMsg}</Txt>}
      </V>
    )
  }
}