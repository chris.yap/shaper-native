import React from 'react';
import { Switch, SwitchProps } from 'react-native';

interface ToggleSwitchProps extends SwitchProps {}

const ToggleSwitch: React.FC<ToggleSwitchProps> = ({ onValueChange, value, testID, ...props }) => {
  return (
    <Switch
      trackColor={{ false: '#d4d9dc', true: '#4c626c' }}
      ios_backgroundColor="#d4d9dc"
      value={value}
      onValueChange={onValueChange}
      testID={testID}
      {...props}
    />
  );
};

export default ToggleSwitch;
