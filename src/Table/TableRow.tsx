import React from 'react';
import { TouchableOpacity } from 'react-native';
import V, { VProps } from '../View';

interface RowProps extends VProps {
  header?: boolean;
  onPress?: () => void;
}

const Row: React.FC<RowProps> = props => (
  <>
    {props.onPress ? (
      <TouchableOpacity onPress={props.onPress}>
        <V
          flexDirection="row"
          borderBottomWidth={props.header ? '2px' : '1px'}
          minHeight={props.header ? 0 : '40px'}
          borderColor={props.header ? 'secondaries.1' : 'secondaries.1'}
          {...props}
        >
          {props.children}
        </V>
      </TouchableOpacity>
    ) : (
      <V
        flexDirection="row"
        borderBottomWidth={props.header ? '2px' : '1px'}
        minHeight={props.header ? 0 : '40px'}
        borderColor={props.header ? 'secondaries.1' : 'secondaries.1'}
        {...props}
      >
        {props.children}
      </V>
    )}
  </>
);

export default Row;
