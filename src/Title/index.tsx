import React from 'react';
import Txt, { TxtProps } from '../Text';

export interface TitleProps extends TxtProps {
  ellipsizeMode?: 'head' | 'middle' | 'tail' | 'clip';
  numberOfLines?: number;
}

const Title: React.FC<TitleProps> = ({ fontFamily, fontSize, ...props }) => (
  <Txt semibold fontSize={fontSize} {...props} />
);

export default Title;

Title.defaultProps = {
  fontSize: '20px',
};
