import React from 'react';
import V, { VProps } from '../View';

export interface CardProps extends VProps {}

const Card: React.FC<CardProps> = props => (
  <V
    bg="white"
    borderTopWidth={1}
    borderLeftWidth={1}
    borderRightWidth={1}
    borderBottomWidth={2}
    borderRadius={2}
    borderColor="secondaries.1"
    overflow="hidden"
    p="1px"
    {...props}
  />
);

export default Card;
