import React from 'react';
import { Animated } from 'react-native';

interface ShowHideProps {
  isOpened: boolean;
  style?: object;
}

interface ShowHideState {
  isOpened?: boolean;
}
class ShowHide extends React.Component<ShowHideProps, ShowHideState> {
  static defaultProps: ShowHideProps = {
    isOpened: false,
  };
  constructor(props: any) {
    super(props);
    this.state = {
      isOpened: props.isOpened,
    };
  }

  _visibility: any = null;

  componentWillMount() {
    this._visibility = new Animated.Value(this.props.isOpened ? 1 : 0);
  }

  componentWillReceiveProps(nextProps: any) {
    if (nextProps.isOpened) {
      this.setState({ isOpened: true });
    }
    Animated.timing(this._visibility, {
      toValue: nextProps.isOpened ? 1 : 0,
      duration: 300,
    }).start(() => {
      this.setState({ isOpened: nextProps.isOpened });
    });
  }

  render = () => {
    const { style, children, ...rest } = this.props;
    const { isOpened } = this.state;
    const containerStyle = {
      opacity: this._visibility.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
      }),
      // transform: [
      //   {
      //     scale: this._visibility.interpolate({
      //       inputRange: [0, 1],
      //       outputRange: [1.1, 1],
      //     }),
      //   },
      // ],
      maxHeight: this._visibility.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 10000],
      }),
    };

    const combinedStyle = [containerStyle, style];
    return (
      <Animated.View style={isOpened ? combinedStyle : containerStyle} {...rest}>
        {isOpened ? children : null}
      </Animated.View>
    );
  };
}

export default ShowHide;
