import React from 'react';
import { ComposedProps } from '../Common/Composed';
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import V from '../View';
import theme, { ThemeProps } from '../Theme';
import ShaperConfig from '../Assets/Fonts/shaper-icons.json';

interface IconProps extends ComposedProps {
  name: string;
  color?: string;
  size?: any;
  style?: object;
  small?: boolean;
  medium?: boolean;
  large?: boolean;
  primary?: boolean;
  secondary?: boolean;
  success?: boolean;
  info?: boolean;
  warning?: boolean;
  danger?: boolean;
  theme?: ThemeProps;
}

const Icon: React.FC<IconProps> = ({ color, name, size, ...props }) => {
  const Ico = createIconSetFromIcoMoon(ShaperConfig);
  return (
    <V alignContent="center" alignItems="center" justifyContent="center" {...props}>
      <Ico
        name={name}
        color={
          props.primary
            ? props.theme && props.theme.colors.primary
            : props.secondary
            ? props.theme && props.theme.colors.secondary
            : props.success
            ? props.theme && props.theme.colors.success
            : props.info
            ? props.theme && props.theme.colors.info
            : props.warning
            ? props.theme && props.theme.colors.warning
            : props.danger
            ? props.theme && props.theme.colors.danger
            : color
            ? color
            : props.theme && props.theme.colors.secondaries[9]
        }
        size={props.small ? 20 : props.medium ? 32 : props.large ? 38 : size}
      />
    </V>
  );
};

export default Icon;

Icon.defaultProps = {
  theme,
  size: 24,
};
