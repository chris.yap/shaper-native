import React from 'react';
import styled from 'styled-components/native';
import { TouchableOpacityProps } from 'react-native';
import { rgba } from 'polished';
import { composed, ComposedProps } from '../Common/Composed';
import theme, { ThemeProps } from '../Theme';
import Icon from '../Icon';

interface BtnWrapperProps extends ComposedProps {
  icon?: string;
  small?: boolean;
  medium?: boolean;
  large?: boolean;
  outlined?: boolean;
  flat?: boolean;
  inverted?: boolean;
  block?: boolean;
  fullwidth?: boolean;
  primary?: boolean;
  secondary?: boolean;
  info?: boolean;
  success?: boolean;
  warning?: boolean;
  danger?: boolean;
  black?: boolean;
  white?: boolean;
  square?: boolean;
  round?: boolean;
  theme?: ThemeProps;
}

const BtnWrapper = styled.TouchableOpacity<BtnWrapperProps>`
  flex-direction: row;
  opacity: ${props => (props.disabled ? 0.2 : 1)};
  height: ${props => (props.small ? '30px' : props.medium ? '44px' : props.large ? '56px' : '38px')};
  width: ${props =>
    props.icon && (props.square || props.round)
      ? props.small
        ? '30px'
        : props.medium
        ? '44px'
        : props.large
        ? '56px'
        : '38px'
      : props.block || props.fullwidth
      ? '100%'
      : 'auto'};
  background-color: ${props =>
    props.flat
      ? 'transparent'
      : props.outlined
      ? 'white'
      : props.inverted
      ? props.primary
        ? rgba(props.theme.colors.primary, 0.1)
        : props.success
        ? rgba(props.theme.colors.success, 0.1)
        : props.info
        ? rgba(props.theme.colors.info, 0.1)
        : props.warning
        ? rgba(props.theme.colors.warning, 0.1)
        : props.danger
        ? rgba(props.theme.colors.danger, 0.1)
        : props.black
        ? rgba(props.theme.colors.black, 0.1)
        : rgba(props.theme.colors.secondary, 0.1)
      : props.primary
      ? props.theme.colors.primary
      : props.secondary
      ? props.theme.colors.secondary
      : props.success
      ? props.theme.colors.success
      : props.info
      ? props.theme.colors.info
      : props.warning
      ? props.theme.colors.warning
      : props.danger
      ? props.theme.colors.danger
      : props.black
      ? props.theme.colors.black
      : props.flat
      ? 'transparent'
      : props.theme.colors.white};
  border-width: ${props =>
    !props.outlined &&
    (props.primary ||
      props.secondary ||
      props.success ||
      props.warning ||
      props.info ||
      props.danger ||
      props.black ||
      props.white ||
      props.flat ||
      props.inverted)
      ? '0'
      : '2px'};
  border-style: solid;
  border-radius: ${props => (props.round ? '290486px' : props.theme.radii[2])};
  border-color: ${props =>
    props.primary
      ? props.theme.colors.primary
      : props.secondary
      ? props.theme.colors.secondary
      : props.success
      ? props.theme.colors.success
      : props.info
      ? props.theme.colors.info
      : props.warning
      ? props.theme.colors.warning
      : props.danger
      ? props.theme.colors.danger
      : props.theme.colors.black};
  align-items: center;
  justify-content: center;
  padding-left: ${props => (props.icon ? '0' : '16px')};
  padding-right: ${props => (props.icon ? '0' : '16px')};
  ${composed};
`;

interface BtnTxtProps extends ComposedProps {
  bold?: boolean;
  semibold?: boolean;
  small?: boolean;
  medium?: boolean;
  large?: boolean;
  outlined?: boolean;
  flat?: boolean;
  inverted?: boolean;
  uppercase?: boolean;
  primary?: boolean;
  secondary?: boolean;
  info?: boolean;
  success?: boolean;
  warning?: boolean;
  danger?: boolean;
  black?: boolean;
  white?: boolean;
  theme: ThemeProps;
}

const BtnTxt = styled.Text<BtnTxtProps>`
  font-family: 'SourceSansPro-Semibold';
  font-size: ${props =>
    props.small
      ? props.theme.fontSizes[6]
      : props.medium
      ? props.theme.fontSizes[8]
      : props.large
      ? props.theme.fontSizes[10]
      : props.theme.fontSizes[7]};
  /* font-weight: ${props => (props.flat ? props.theme.fontWeights.bold : props.theme.fontWeights.semibold)}; */
  text-transform: ${props => (props.uppercase ? 'uppercase' : 'none')};
  color: ${props =>
    props.outlined || props.inverted || props.flat
      ? props.primary
        ? props.theme.colors.primary
        : props.secondary
        ? props.theme.colors.secondary
        : props.success
        ? props.theme.colors.success
        : props.info
        ? props.theme.colors.info
        : props.warning
        ? props.theme.colors.warning
        : props.danger
        ? props.theme.colors.danger
        : props.black
        ? props.theme.colors.secondaries[9]
        : props.white
        ? 'white'
        : props.theme.colors.secondaries[9]
      : props.primary || props.secondary || props.success || props.info || props.warning || props.danger || props.black
      ? 'white'
      : props.theme.colors.secondaries[9]};
  ${composed};
`;

interface BtnProps extends ComposedProps, TouchableOpacityProps {
  icon?: string;
  small?: boolean;
  medium?: boolean;
  large?: boolean;
  outlined?: boolean;
  flat?: boolean;
  inverted?: boolean;
  block?: boolean;
  fullwidth?: boolean;
  primary?: boolean;
  secondary?: boolean;
  info?: boolean;
  success?: boolean;
  warning?: boolean;
  danger?: boolean;
  black?: boolean;
  white?: boolean;
  square?: boolean;
  round?: boolean;
  uppercase?: boolean;
  textProps?: object;
  onPress?: any;
  flex?: number;
  flexGrow?: number;
  flexShrink?: number;
  justifyContent?: string;
  alignContent?: string;
  alignItems?: string;
  theme?: ThemeProps;
}

const Btn: React.FC<BtnProps> = ({ textProps, theme, ...props }) => {
  return (
    <BtnWrapper onPress={!props.disabled ? props.onPress : undefined} {...props}>
      {props.icon && (
        <Icon
          name={props.icon}
          size={props.small ? 20 : props.medium ? 32 : props.large ? 38 : 24}
          color={
            props.inverted || props.outlined || props.flat
              ? props.primary
                ? theme && theme.colors.primary
                : props.secondary
                ? theme && theme.colors.secondary
                : props.success
                ? theme && theme.colors.success
                : props.info
                ? theme && theme.colors.info
                : props.warning
                ? theme && theme.colors.warning
                : props.danger
                ? theme && theme.colors.danger
                : props.black
                ? 'white'
                : theme && theme.colors.secondaries[9]
              : props.primary ||
                props.secondary ||
                props.success ||
                props.info ||
                props.warning ||
                props.danger ||
                props.black
              ? 'white'
              : theme && theme.colors.secondaries[9]
          }
        />
      )}
      {props.children && (
        <BtnTxt
          bold={props.flat}
          semibold={!props.flat}
          small={props.small}
          medium={props.medium}
          large={props.large}
          primary={props.primary}
          secondary={props.secondary}
          info={props.info}
          success={props.success}
          warning={props.warning}
          danger={props.danger}
          black={props.black}
          outlined={props.outlined}
          inverted={props.inverted}
          flat={props.flat}
          uppercase={props.uppercase}
          mx={props.icon ? 1 : 0}
          {...textProps}
        >
          {props.children}
        </BtnTxt>
      )}
    </BtnWrapper>
  );
};

export default Btn;

Btn.defaultProps = {
  theme,
};
