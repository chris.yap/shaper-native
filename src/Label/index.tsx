import React from 'react';
import Txt, { TxtProps } from '../Text';

export interface LabelProps extends TxtProps {
  required?: boolean;
}

const Label: React.FC<LabelProps> = props => (
  <Txt fontSize={5} uppercase color="secondary" {...props}>
    {props.children} {props.required && '*'}
  </Txt>
);

export default Label;
