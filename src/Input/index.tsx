import React from 'react';
import { Platform, TouchableOpacity, TextInputProps } from 'react-native';
import styled from 'styled-components/native';
import { composed } from '../Common/Composed';
import Icon from '../Icon';
import Label from '../Label';
import Txt from '../Text';
import V from '../View';
import { ThemeProps } from '../Theme';

interface InputWrapperProps {
  icon?: string;
  error?: boolean;
  theme: ThemeProps;
}

interface InputFieldProps extends TextInputProps {
  icon?: string;
  inputProps?: object;
  label?: any;
  required?: boolean;
  password?: boolean;
  error?: boolean;
  errorMessage?: any;
  testID?: string;
  theme?: ThemeProps;
  justifyContent?: any;
  useRef?: any;
}

const InputWrapper = styled.TextInput<InputWrapperProps>`
  border: ${props => `1px solid ${props.error ? props.theme.colors.danger : props.theme.colors.blacks[2]}`};
  background-color: ${props => (!props.editable ? props.theme.colors.secondaries[1] : 'white')};
  border-radius: ${props => props.theme.radii[2]};
  height: 38px;
  padding-left: ${props => (props.icon ? '36px' : '8px')};
  padding-right: 8px;
  font-size: ${props => `${props.theme.fontSizes[7]}`};
  color: ${props => (!props.editable ? props.theme.colors.secondaries[5] : props.theme.colors.black)};
  ${composed};
`;

const InputField: React.FC<InputFieldProps> = ({ inputProps, label, required, testID, ...props }) => {
  const [inputState, setInputState] = React.useState<{
    showPassword: null | boolean;
  }>({ showPassword: props.password || null });

  const setShowPassword = (showPassword: boolean | null) => {
    setInputState({ showPassword: showPassword ? false : true });
  };
  return (
    <React.Fragment>
      {label && <Label>{label}</Label>}
      <V position="relative" style={{ justifyContent: 'center' }}>
        {props.icon && (
          <Icon
            flex={0}
            name={props.icon}
            zIndex={10}
            size={20}
            color={props.error ? 'red' : 'black'}
            opacity={0.5}
            position="absolute"
            fontSize={24}
            width={36}
            style={{ alignItems: 'center' }}
          />
        )}
        {props.password && (
          <TouchableOpacity
            style={{
              position: 'absolute',
              right: Platform.OS === 'ios' ? 26 : 0,
              zIndex: 100,
              padding: 10,
              top: 0,
              flexDirection: 'row',
            }}
            onPress={() => setShowPassword(inputState.showPassword)}
            testID={!inputState.showPassword ? 'hide' : 'show'}
          >
            <Txt
              fontFamily="SourceSansPro-Bold"
              fontSize={5}
              color={props.error ? 'dangers.3' : 'blacks.3'}
              // mr={isCapsLockActive ? 4 : 0}
            >
              {!inputState.showPassword ? 'HIDE' : 'SHOW'}
            </Txt>
          </TouchableOpacity>
        )}
        <InputWrapper
          error={props.error}
          testID={testID}
          icon={props.icon}
          {...inputProps}
          secureTextEntry={inputState.showPassword}
          ref={props.useRef}
          {...props}
        />
      </V>
      {props.error && props.errorMessage && (
        <V mt={1}>
          {Array.isArray(props.errorMessage) ? (
            props.errorMessage.map((message, m) => (
              <Txt key={m} color="danger" lineHeight="16px" pt="2px">
                {message}
              </Txt>
            ))
          ) : (
            <Txt color="danger" lineHeight="16px" pt="2px">
              {props.errorMessage}
            </Txt>
          )}
        </V>
      )}
    </React.Fragment>
  );
};

export default InputField;
