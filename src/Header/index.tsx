import React from 'react';
import { StatusBar } from 'react-native';
import { useSafeArea } from 'react-native-safe-area-context';
import V, { VProps } from '../View';

export interface HeaderProps extends VProps {
  bg?: string;
  dark?: boolean;
  left?: any;
  title?: any;
  right?: any;
}

const Header: React.FC<HeaderProps> = ({ bg, left, title, right, dark, ...props }) => {
  const insets = useSafeArea();
  return (
    <V bg={bg} p={0} {...props}>
      <StatusBar barStyle={dark ? 'light-content' : 'dark-content'} translucent={false} />
      <V pt={insets.top}>
        <V flexDirection="row" px={1} pb={!!left || !!title || !!right ? '4px' : 0}>
          <V justifyContent="center" alignItems="flex-start" minWidth={'60px'}>
            {left}
          </V>
          <V flexGrow={1} flexShrink={1} mx={2} justifyContent="center" alignItems="center">
            {title}
          </V>
          <V justifyContent="center" alignItems="flex-end" minWidth={'60px'}>
            {right}
          </V>
        </V>
      </V>
    </V>
  );
};

export default Header;
