import styled from 'styled-components/native';
import { FlatList } from 'react-native';
import { composed, ComposedProps } from '../Common/Composed';
import theme from '../Theme';

interface ListProps extends ComposedProps {}

const List = styled(FlatList as new () => FlatList<ListProps>)`
  ${composed};
`;

export default List;

List.defaultProps = {
  theme,
};
