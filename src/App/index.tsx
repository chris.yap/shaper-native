import React from 'react';
import { ThemeProvider } from 'styled-components/native';
import theme from '../Theme';

const defaultTheme = theme;

interface AppProps {
  theme: object;
}

const App: React.FC<AppProps> = props => {
  return <ThemeProvider theme={defaultTheme} {...props} />;
};

export default App;
