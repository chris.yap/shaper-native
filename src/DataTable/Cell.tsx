import React from 'react';
import V, { VProps } from '../View';

export interface CellProps extends VProps {
  align?: 'right' | 'center' | 'left';
  style?: object;
  width?: number;
}

const Cell: React.FC<CellProps> = props => {
  return (
    <V
      py={2}
      px={4}
      width={props.width}
      justifyContent={props.align === 'right' ? 'flex-end' : props.align === 'center' ? 'center' : 'flex-start'}
      flexDirection="row"
    >
      {props.children}
    </V>
  );
};
export default Cell;
