import React from 'react';
import { ScrollView } from 'react-native';
// import { Table } from 'react-native-table-component';
import TableHead from './TableHead';
import V from '../View';

interface DataTableProps {
  headers?: Array<object>;
  colWidths?: Array<number>;
  data?: Array<any>;
  TableRow?: any;
}

const DataTable: React.FC<DataTableProps> = ({ headers, colWidths, data, TableRow, ...props }) => {
  return (
    <V {...props}>
      <ScrollView horizontal nestedScrollEnabled>
        <V flex={0}>
          {headers && <TableHead data={headers} colWidths={colWidths} />}
          <V maxHeight="500px">
            <ScrollView style={{ flexGrow: 1 }}>
              {data && data.map((item, i) => <TableRow key={i} item={item} index={i} colWidths={colWidths} />)}
            </ScrollView>
          </V>
        </V>
      </ScrollView>
    </V>
  );
};

export default DataTable;
