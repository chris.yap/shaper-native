import React from 'react';
// import { Cell, Row, TableWrapper } from 'react-native-table-component';
// import { FlatList } from 'react-native';
import { ComposedProps } from '../Common/Composed';
import Icon from '../Icon';
import Txt from '../Text';
import V from '../View';
import Row from './Row';

interface TableHeadProps extends ComposedProps {
  data?: Array<object>;
  colWidths?: Array<number>;
  style?: object;
}

interface ItemProps {
  index: number;
  colWidths?: Array<number>;
  item: {
    align?: string;
    label?: string;
    sort?: any;
  };
}

const TableHeader: React.FC<ItemProps> = ({ index, colWidths, item }) => (
  <V
    width={colWidths && colWidths[index]}
    py={2}
    px={3}
    flexDirection="row"
    alignItems="center"
    justifyContent={
      item.align && item.align === 'right' ? 'flex-end' : item.align === 'center' ? 'center' : 'flex-start'
    }
  >
    {item.sort && <Icon name="sort" size={19} />}
    <Txt fontFamily="SourceSansPro-Bold" fontSize={6}>
      {item.label}
    </Txt>
  </V>
);

const TableHead: React.FC<TableHeadProps> = ({ colWidths, data, ...props }) => {
  return (
    <Row bg="white" borderBottomWidth="2px" borderColor="#999" {...props}>
      {data && data.map((item, index) => <TableHeader key={index} index={index} colWidths={colWidths} item={item} />)}
    </Row>
    // <FlatList
    //   horizontal
    //   stickyHeaderIndices={[0]}
    //   data={data}
    //   renderItem={({ item, index }: { item: ItemProps; index: number }) => (
    //     <TableHeader item={item} index={index} colWidths={colWidths} />
    //   )}
    //   keyExtractor={({ item, index }) => 'key' + index}
    //   style={{ borderBottomWidth: 3, borderColor: '#999' }}
    // />
  );
};

export default TableHead;

TableHead.defaultProps = {
  data: [],
};
