import styled from 'styled-components/native';
import { composed, ComposedProps } from '../Common/Composed';

interface RowProps extends ComposedProps {
  index?: number;
}

const Row = styled.View<RowProps>`
  flex-direction: row;
  min-height: 40px;
  align-items: center;
  border-bottom-width: 1px;
  border-color: #ddd;
  background-color: ${props => (props.index && props.index % 2 == 1 ? '#fff' : '#f2f2f2')};
  ${composed};
`;

export default Row;
