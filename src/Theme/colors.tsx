import { shade, tint } from 'polished';

const primaryColors = { red: '#c20000', blueGrey: '#4c626c', black: '#121212' };
const secondaryColors = {
  sea: '#152773',
  blueberry: '#572381',
  sky: '#005aa3',
  lime: '#a4b123',
  tangerine: '#e9600e',
};
const contextual = {
  // success: '#018701',
  // info: '#005aa3',
  // warning: '#e9600e',
  // danger: '#ce3030'
  success: '#037d04',
  info: '#005aa3',
  warning: '#e9600e',
  danger: '#d20000',
};

const getShades = (color: string, steps: number) => {
  let lights = [];
  let darks = [];
  for (let i = 1; i <= steps; i++) {
    const col = tint(i * 0.19, color);
    lights.push(col);
  }
  for (let i = 1; i <= steps; i++) {
    const col = shade(i * 0.1, color);
    darks.push(col);
  }
  lights = lights.reverse();
  lights.push(color);
  return lights.concat(darks);
};

const colors = {
  text: '#363636',

  black: primaryColors.black,
  blacks: getShades(primaryColors.black, 5),
  white: '#fff',
  primary: primaryColors.red,
  secondary: primaryColors.blueGrey,

  red: primaryColors.red,
  reds: getShades(primaryColors.red, 5),
  primaries: getShades(primaryColors.red, 5),

  bluegrey: primaryColors.blueGrey,
  bluegreys: getShades(primaryColors.blueGrey, 5),
  secondaries: getShades(primaryColors.blueGrey, 5),

  sea: secondaryColors.sea,
  seas: getShades(secondaryColors.sea, 5),

  blueberry: secondaryColors.blueberry,
  blueberries: getShades(secondaryColors.blueberry, 5),

  sky: secondaryColors.sky,
  skies: getShades(secondaryColors.sky, 5),

  lime: secondaryColors.lime,
  limes: getShades(secondaryColors.lime, 5),

  tangerine: secondaryColors.tangerine,
  tangerines: getShades(secondaryColors.tangerine, 5),

  success: contextual.success,
  successes: getShades(contextual.success, 5),

  info: contextual.info,
  infos: getShades(contextual.info, 5),

  warning: contextual.warning,
  warnings: getShades(contextual.warning, 5),

  danger: contextual.danger,
  dangers: getShades(contextual.danger, 5),
};

export { colors };
