import React from 'react';
import V, { VProps } from '../View';
import Txt from '../Text';

interface TagProps extends VProps {
  black?: boolean;
  white?: boolean;
  success?: boolean;
  warning?: boolean;
  danger?: boolean;
  small?: boolean;
  medium?: boolean;
  large?: boolean;
}

const Tag: React.FC<TagProps> = ({ ...props }) => (
  <V
    bg={
      props.success
        ? 'success'
        : props.warning
        ? 'warning'
        : props.danger
        ? 'danger'
        : props.black
        ? 'secondaries.9'
        : props.white
        ? 'white'
        : 'info'
    }
    justifyContent="center"
    px={1}
    borderRadius="290486px"
    alignSelf="flex-start"
    {...props}
  >
    <Txt
      color={props.white ? 'secondaries.9' : 'white'}
      fontSize={props.small ? 2 : props.medium ? 5 : props.large ? 7 : 4}
      semibold
    >
      {props.children}
    </Txt>
  </V>
);

export default Tag;
