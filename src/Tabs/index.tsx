import React from 'react';
import { TouchableWithoutFeedback, Dimensions } from 'react-native';
import { TabView, TabBar, NavigationState, SceneRendererProps } from 'react-native-tab-view';
import Animated from 'react-native-reanimated';
import Icon from '../Icon';
import V from '../View';
import Txt from '../Text';
import { customIconTabBarStyles, autoWidthTabBarStyles } from './TabsStyle';
import theme from '../Theme';

interface Props {
  routes: Route[];
  customIconTabBar?: boolean;
  tabBarPositionBottom?: string;
  enableLazy?: boolean;
  lazyPreloadDistance?: number;
  focused?: boolean;
  enableSwipe?: boolean;
  getTabComponent: (key: any) => any;
  runTabCallBack: (key: any) => void;
  getTabKeyByIndex: () => void;
}

type Route = {
  key: string;
  title: string;
  icon?: string;
};

type State = NavigationState<Route>;
const initialLayout = { width: Dimensions.get('window').width };

export default class Tabs extends React.Component<Props, State> {
  state = {
    index: 0,
    routes: this.props.routes,
    // routes: [
    //   { key: 'contacts', title: 'Contacts', icon: 'ios-people' },
    // ],
  };

  private getTabKeyByIndex = (index: number) => this.props.routes[index].key;

  private handleIndexChange = (index: number) => {
    this.setState({
      index,
    });
    this.props.runTabCallBack(this.getTabKeyByIndex(index));
  };

  private renderItem = ({
    navigationState,
    position,
  }: {
    navigationState: State;
    position: Animated.Node<number>;
  }) => ({ route, index }: { route: Route; index: number }) => {
    const inputRange = navigationState.routes.map((_, i) => i);

    const activeOpacity = Animated.interpolate(position, {
      inputRange,
      outputRange: inputRange.map((i: number) => (i === index ? 1 : 0)),
    });
    const inactiveOpacity = Animated.interpolate(position, {
      inputRange,
      outputRange: inputRange.map((i: number) => (i === index ? 0 : 1)),
    });

    return (
      <V style={customIconTabBarStyles.tab}>
        <Animated.View style={[customIconTabBarStyles.item, { opacity: inactiveOpacity }]}>
          {route.icon && (
            <Icon
              name={route.icon}
              size={'small'}
              style={[customIconTabBarStyles.icon, customIconTabBarStyles.inactive]}
            />
          )}
          <Txt style={[customIconTabBarStyles.label, customIconTabBarStyles.inactive]}>{route.title}</Txt>
        </Animated.View>

        {/* active tab */}
        <Animated.View
          style={[customIconTabBarStyles.item, customIconTabBarStyles.activeItem, { opacity: activeOpacity }]}
        >
          {route.icon && (
            <Icon
              name={route.icon}
              size={'small'}
              style={[customIconTabBarStyles.icon, customIconTabBarStyles.inactive]}
            />
          )}
          <Txt style={[customIconTabBarStyles.label, customIconTabBarStyles.active]}>{route.title}</Txt>
        </Animated.View>
      </V>
    );
  };

  private customIconTabBar = (props: SceneRendererProps & { navigationState: State }) => (
    <V style={customIconTabBarStyles.tabbar}>
      {props.navigationState.routes.map((route: Route, index: number) => {
        return (
          <TouchableWithoutFeedback key={route.key} onPress={() => props.jumpTo(route.key)}>
            {this.renderItem(props)({ route, index })}
          </TouchableWithoutFeedback>
        );
      })}
    </V>
  );

  private autoWidthTabBar = (props: SceneRendererProps & { navigationState: State }) => (
    //TO DO: update with styled components//
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={autoWidthTabBarStyles.indicator}
      style={autoWidthTabBarStyles.tabbar}
      labelStyle={autoWidthTabBarStyles.label}
      tabStyle={autoWidthTabBarStyles.tabStyle}
      renderLabel={({ route, focused }) => (
        <Txt semibold color={focused ? theme.colors.primary : theme.colors.black}>
          {route.title}
        </Txt>
      )}
    />
  );

  private renderScene = ({ route }: { route: Route }) => {
    const tab = this.props.getTabComponent(route.key);
    // if (this.state.index > 2 && Math.abs(this.state.index - this.props.routes.indexOf(route)) > 2) {
    //   return <V />;
    // }
    return <V>{tab}</V>;
  };

  render() {
    const { customIconTabBar, tabBarPositionBottom, enableLazy, lazyPreloadDistance, enableSwipe } = this.props;
    return (
      <TabView
        //style={{ height: 620 }}
        navigationState={this.state}
        renderScene={this.renderScene}
        renderTabBar={customIconTabBar ? this.customIconTabBar : this.autoWidthTabBar}
        tabBarPosition={tabBarPositionBottom ? 'bottom' : 'top'}
        onIndexChange={this.handleIndexChange}
        initialLayout={initialLayout}
        lazy={enableLazy ? true : false}
        lazyPreloadDistance={lazyPreloadDistance}
        swipeEnabled={enableSwipe ? true : false}
      />
    );
  }
}
