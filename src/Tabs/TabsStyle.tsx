import { StyleSheet } from 'react-native';
import theme from '../Theme';

export const autoWidthTabBarStyles = StyleSheet.create({
  tabbar: {
    backgroundColor: theme.colors.white,
  },
  indicator: {
    backgroundColor: theme.colors.primary,
  },
  label: {
    fontWeight: '400',
    color: theme.colors.black,
  },
  tabStyle: {
    width: 'auto',
  },
});

// TO DO: create icon tab styles//
export const customIconTabBarStyles = StyleSheet.create({
  tabbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fafafa',
  },
  tab: {
    flex: 1,
    alignItems: 'center',
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: 'rgba(0, 0, 0, .2)',
  },
  item: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 4.5,
  },
  activeItem: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  active: {
    color: '#0084ff',
  },
  inactive: {
    color: '#939393',
  },
  icon: {
    height: 26,
    width: 26,
  },
  label: {
    fontSize: 10,
    marginTop: 3,
    marginBottom: 1.5,
    backgroundColor: 'transparent',
  },
});
