import styled from 'styled-components/native';
import { composed, ComposedProps } from '../Common/Composed';
import theme, { ThemeProps } from '../Theme';

interface DividerProps extends ComposedProps {
  dark?: boolean;
  theme: ThemeProps;
}

const Divider = styled.View<DividerProps>`
  border-bottom-width: 1px;
  border-style: solid;
  border-bottom-color: ${props => (props.dark ? 'rgba(255,255,255,.22)' : props.theme.colors.secondaries[1])};
  opacity: 0.5;
  ${composed};
`;

export default Divider;

Divider.defaultProps = {
  theme,
};
