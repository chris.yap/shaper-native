import React from 'react';
import Btn from '../Button';
import V from '../View';

export interface BtnGroupProps {
  justify?: boolean;
  round?: boolean;
  buttons?: Array<string>;
  selectedIndex?: number;
  onBtnPress: (index: number) => void;
}

const BtnGroup: React.FC<BtnGroupProps> = ({ justify, round, buttons, selectedIndex, ...props }) => {
  const ButtonPress = (index: number) => {
    props.onBtnPress(index);
  };
  return (
    <V
      flexDirection="row"
      alignSelf="center"
      borderRadius={round ? '290486px' : 2}
      bg="white"
      position="relative"
      {...props}
    >
      {buttons
        ? buttons.map((btn: string, b: number) => (
            <Btn
              key={b}
              black={!!(selectedIndex === b)}
              flat={!!(selectedIndex !== b)}
              flex={justify ? 1 : undefined}
              borderWidth="0"
              borderRadius={round ? '290486px' : 2}
              onPress={() => ButtonPress(b)}
              flexGrow={1}
            >
              {btn}
            </Btn>
          ))
        : props.children}
    </V>
  );
};

export default BtnGroup;
